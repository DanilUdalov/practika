class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :require_admin

  def require_admin
    redirect_to root_path unless session[:admin]
  end
end
