class ItemsController < ApplicationController
  before_action :item_resource, only: [:edit, :show, :update, :destroy]

  def index
    @items = Item.order(:id)
  end

  def new
    @item = Item.new
  end

  def update
    if @item.update(item_params)
      redirect_to items_path
    else
      render :edit
    end
  end

  def create
    @item = Item.new(item_params)
    if @item.save
      redirect_to items_path
    else
      render :new
    end
  end

  def destroy
    @item.destroy
  end

  private

  def item_resource
    @item = Item.find(params[:id])
  end

  def item_params
    params.require(:item).permit(:name, :category)
  end
end
