class PricelistsController < ApplicationController
  before_action :price_resource, only: [:edit, :show, :update, :destroy]

  def index
    @pricelists = Pricelist.order(:id)
  end

  def new
    @pricelist = Pricelist.new
  end

  def edit
  end

  def show
  end

  def update
    if @pricelist.update(pricelist_params)
      redirect_to @pricelist
    else
      render :edit
    end
  end

  def create
    @pricelist = Pricelist.new(pricelist_params)
    if @pricelist.save
      redirect_to @pricelist
    else
      render :new
    end
  end

  def destroy
    @pricelist.destroy
    redirect_to pricelist_path
  end

  private

  def price_resource
    @pricelist = Pricelist.find(params[:id])
  end

  def pricelist_params
    params.require(:pricelist).permit(:name, :recipient, positions_attributes: [:name, :price, :count, :_destroy])
  end
end
