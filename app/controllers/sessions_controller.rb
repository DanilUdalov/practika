class SessionsController < ApplicationController
  skip_before_action :require_admin, only: [:new, :create]

  def create
    if params[:email] == ENV['admin_email'] && params[:password] == ENV['admin_password']
      session[:admin] = true
      redirect_to pricelists_path
    else
      render :new
    end
  end

  def destroy
    session[:admin] = false
    redirect_to root_path
  end
end
