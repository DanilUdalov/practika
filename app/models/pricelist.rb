class Pricelist < ApplicationRecord
  has_many :positions, dependent: :delete_all

  accepts_nested_attributes_for :positions, allow_destroy: true
end
