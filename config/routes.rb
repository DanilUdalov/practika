Rails.application.routes.draw do
  get 'items/new'

  get 'items/index'

  root 'sessions#new'
  get 'logout', to: 'sessions#destroy'
  get 'login', to: 'sessions#new', as: 'login'
  resources :sessions, only: [:new, :create, :destroy]
  resources :pricelists do
    resources :positions
  end
  resources :items
  root 'pricelists#index'
end
