class CreatePricelists < ActiveRecord::Migration[5.0]
  def change
    create_table :pricelists do |t|
      t.string :name
      t.string :recipient

      t.timestamps
    end
  end
end
